# Resources

Here are some great resources for learning or improving Ruby skills:

* IRC: ##new2ruby on freenode
* [TryRuby.org](http://tryruby.org)
* [why's (poignant) guide to Ruby](http://www.rubyinside.com/media/poignant-guide.pdf)
* [RubyWarrion](https://www.bloc.io/ruby-warrior/#/)
* [Ruby Koans](http://www.rubykoans.com)
* [Kids Ruby](http://www.kidsruby.com)
* [The Odin Project](http://www.theodinproject.com/ruby-programming)
* [Ruby Kickstart](http://www.ruby-kickstart.com)
* [Ruby Regular Expression Tester](http://rubular.com)
* [Scuttle a SQL and Arel Editor](http://www.scuttle.io/)
