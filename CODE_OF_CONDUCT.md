# Code of Conduct

The ##new2ruby irc channel is dedicated to providing a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, or religion. We do not tolerate harassment of participants in any form. Sexual language and imagery is not appropriate without discussion and pre-approval from a moderator. Participants violating these rules may be sanctioned or expelled from the group at the discretion of @Coraline or another moderator.

Harassment includes offensive verbal comments related to gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, religion, sexual images in public spaces, deliberate intimidation, stalking, following, harassing photography or recording, sustained disruption of discussion, inappropriate contact, and unwelcome sexual attention.

Participants asked to stop any harassing behavior are expected to comply immediately.

If a participant engages in harassing behavior, @Coraline or another moderator may take any action they deem appropriate, including warning the offender or permanent expulsion from the channel.

If you are being harassed, notice that someone else is being harassed, or have any other concerns and no channel operator is available, please contact Coraline at coraline@idolhands.com as soon as possible. She will address the issue as soon as she is able to.

# Additional Guidelines

* Participants may not send a private message to any other channel member before publicly asking for permission to do so first. 

* Participants may not solicit or advertise their services to other channel members.

* The channel may not be used to solicit, advertise, or distribute copyrighted  materials.